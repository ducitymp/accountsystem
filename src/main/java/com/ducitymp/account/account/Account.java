package com.ducitymp.account.account;

import org.bson.Document;

import java.util.UUID;

public class Account {

    private final UUID uuid;

    private Document document;

    public Account(UUID uuid, Document document) {
        this.uuid = uuid;
        this.document = document;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setName(String name) {
        document.put("name", name);
    }

    public Long getFirstLogin() {
        return document.getLong("firstlogin");
    }

    public Document getDocument() {
        return document;
    }
}
