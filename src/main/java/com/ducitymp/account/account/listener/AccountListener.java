package com.ducitymp.account.account.listener;

import com.ducitymp.account.AccountPlugin;
import com.ducitymp.account.account.Account;
import org.bson.Document;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

public class AccountListener implements Listener {

    private final AccountPlugin plugin;

    public AccountListener(AccountPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void on(AsyncPlayerPreLoginEvent event) {
        Account account = plugin.getAccountManager().getAccount(event.getUniqueId(), false);

        if(account == null) {
            account = plugin.getAccountManager().getAccount(event.getUniqueId(), true);

            if(account == null) {
                Document document = new Document();
                document.put("uuid", event.getUniqueId().toString());
                document.put("name", event.getName());
                document.put("firstlogin", System.currentTimeMillis());

                account = new Account(event.getUniqueId(), document);

                plugin.getAccountManager().insertInDatabase(account);
            }
        }

        account.setName(event.getName());

        plugin.getAccountManager().getAccounts().add(account);
        plugin.getAccountManager().updateInCache(account);
    }

}
