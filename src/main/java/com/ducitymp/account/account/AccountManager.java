package com.ducitymp.account.account;

import com.ducitymp.account.AccountPlugin;
import com.ducitymp.account.Module;
import com.ducitymp.account.account.listener.AccountListener;
import com.ducitymp.rediscache.RedisCacheBuilder;
import com.ducitymp.rediscache.cache.RedisCache;
import com.ducitymp.rediscache.connection.ConnectionData;
import com.google.common.collect.Sets;
import com.mongodb.client.FindIterable;
import org.bson.Document;

import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;

public class AccountManager extends Module {

    public static final String ACCOUNT_KEY_CACHE = "accounts";

    private final Set<Account> accounts = Sets.newHashSet();
    private final RedisCache accountCache;

    public AccountManager(AccountPlugin plugin) {
        super(plugin);

        registerListeners(new AccountListener(plugin));

        RedisCacheBuilder accountCacheBuilder = new RedisCacheBuilder().setConnectionData(new ConnectionData("127.0.0.1", 6379));

        this.accountCache = accountCacheBuilder.buildCache();
    }

    public Account getAccount(UUID uuid, Boolean database) {
        for(Account account : accounts) {
            if(account.getUuid().equals(uuid)) {
                return account;
            }
        }

        if(database) {
            FindIterable<Document> result = getPlugin(). getDatabaseManager().getMongoDatabase().getCollection("accounts").find(new Document("uuid", uuid.toString()));

            Document first = result.first();

            if(first != null) {
                Account account = new Account(uuid, first);

                return account;
            }

            return null;
        } else {
            Supplier<Object> data = accountCache.getData(ACCOUNT_KEY_CACHE, uuid.toString(), String.class);

            if(data != null) {
                Document document = Document.parse((String) data.get());
                Account account = new Account(uuid, document);

                return account;
            }

            return null;
        }
    }

    public void updateInCache(Account account) {
        accountCache.setData(ACCOUNT_KEY_CACHE, account.getUuid().toString(), account.getDocument().toJson());
    }

    public void insertInDatabase(Account account) {
        getPlugin().getDatabaseManager().getMongoDatabase().getCollection("accounts").insertOne(account.getDocument());
    }

    public Set<Account> getAccounts() {
        return accounts;
    }
}
