package com.ducitymp.account;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.stream.Stream;

public abstract class Module {

    private final AccountPlugin plugin;

    public Module(AccountPlugin plugin) {
        this.plugin = plugin;
    }

    public void registerListeners(Listener... listeners) {
        Stream.of(listeners).forEach(listener -> Bukkit.getPluginManager().registerEvents(listener, plugin));
    }

    public AccountPlugin getPlugin() {
        return plugin;
    }
}
