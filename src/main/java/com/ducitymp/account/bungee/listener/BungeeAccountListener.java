package com.ducitymp.account.bungee.listener;

import com.ducitymp.account.account.AccountManager;
import com.ducitymp.account.bungee.AccountBungeePlugin;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.bson.Document;

import java.util.function.Supplier;

public class BungeeAccountListener implements Listener {

    private final AccountBungeePlugin plugin;

    public BungeeAccountListener(AccountBungeePlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void on(PlayerDisconnectEvent event) {
        Supplier<Object> data = plugin.getAccountCache().getData(AccountManager.ACCOUNT_KEY_CACHE, event.getPlayer().getUniqueId().toString(), String.class);

        if (data.get() != null) {
            new Thread(() -> {
                Document document = Document.parse((String) data.get());

                plugin.getDatabaseManager().getMongoDatabase().getCollection("accounts").updateOne(new Document("uuid", event.getPlayer().getUniqueId().toString()), new Document("$set", document));
                plugin.getAccountCache().removeData(AccountManager.ACCOUNT_KEY_CACHE, event.getPlayer().getUniqueId().toString());
            }).run();
        }

    }

}
