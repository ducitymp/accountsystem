package com.ducitymp.account.bungee;

import com.ducitymp.account.bungee.database.DatabaseManager;
import com.ducitymp.account.bungee.listener.BungeeAccountListener;
import com.ducitymp.rediscache.RedisCacheBuilder;
import com.ducitymp.rediscache.cache.RedisCache;
import com.ducitymp.rediscache.connection.ConnectionData;
import net.md_5.bungee.api.plugin.Plugin;

public class AccountBungeePlugin extends Plugin {

    private RedisCache accountCache;

    private DatabaseManager databaseManager;

    @Override
    public void onEnable() {
        RedisCacheBuilder accountCacheBuilder = new RedisCacheBuilder().setConnectionData(new ConnectionData("127.0.0.1", 6379));
        this.accountCache = accountCacheBuilder.buildCache();

        databaseManager = new DatabaseManager();

        getProxy().getPluginManager().registerListener(this, new BungeeAccountListener(this));

    }

    public RedisCache getAccountCache() {
        return accountCache;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }
}
