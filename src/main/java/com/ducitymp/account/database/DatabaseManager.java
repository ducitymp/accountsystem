package com.ducitymp.account.database;

import com.ducitymp.account.AccountPlugin;
import com.ducitymp.account.Module;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

import java.util.Arrays;

public class DatabaseManager extends Module {

    private final MongoClient mongoClient;
    private final MongoDatabase mongoDatabase;

    private final String host = "127.0.0.1";
    private final Integer port = 27017;
    private final String databaseName = "ducitymp";
    private final String username = "ducitymp";
    private final String password = "password";

    public DatabaseManager(AccountPlugin plugin) {
        super(plugin);

        mongoClient = new MongoClient(new ServerAddress(host, port), Arrays.asList(MongoCredential.createCredential(username, databaseName, password.toCharArray())));
        mongoDatabase = mongoClient.getDatabase(databaseName);
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public MongoDatabase getMongoDatabase() {
        return mongoDatabase;
    }
}
