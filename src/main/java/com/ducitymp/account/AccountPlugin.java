package com.ducitymp.account;

import com.ducitymp.account.account.AccountManager;
import com.ducitymp.account.database.DatabaseManager;
import org.bukkit.plugin.java.JavaPlugin;

public class AccountPlugin extends JavaPlugin {

    private DatabaseManager databaseManager;
    private AccountManager accountManager;

    @Override
    public void onEnable() {
        this.databaseManager = new DatabaseManager(this);
        this.accountManager = new AccountManager(this);
    }

    @Override
    public void onDisable() {

    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }
}
